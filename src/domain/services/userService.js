const userRepo = require('../../infrastructure/repositories/userRepo')



module.exports = function(){

    function getAll(){
       return userRepo.getAll();
    }

    return {
        getAll : getAll
    }
}()