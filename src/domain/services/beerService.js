const beerRepo = require("../../infrastructure/repositories/beerRepo");

module.exports = (function() {
  function getAll() {
    return beerRepo.getAll();
  }

  function getBoxPrice(id, quantity) {
    return beerRepo.getBoxPrice(id,quantity)
  }

  function getById(id){
    return beerRepo.getById(id)
  }

  function insert(cerveza){
    beerRepo.insert(cerveza)
  }

  function remove(id){
    beerRepo.remove(id)
  }

  return {
    getAll: getAll,
    getBoxPrice: getBoxPrice,
    getById: getById,
    insert: insert,
    remove: remove
  };
})();
