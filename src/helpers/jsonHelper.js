
exports.JsonHelper = function ()
{
  function jsonCopy(src) {
    return JSON.parse(JSON.stringify(src));
  }

  return {
    jsonCopy : jsonCopy
  }
}
