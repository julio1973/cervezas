const rp = require('request-promise');

const checkUserPermissions = async (request, userContextData) => {
  if (!request || !userContextData)
    throw new Error("Faltan datos requeridos");

  const personaId = userContextData.personaId;
  const routePath = request.route.path.source ?
    request.route.path.source :
    request.route.path;

  const userFunctions = await getUserFunctions(personaId, request.token);
  const hasPermissions = checkIfUserHassPermissions(userFunctions,
    routePath,
    request.method);

  return hasPermissions;
}

async function getUserFunctions(personaId, token) {
  try {
    const options = getUserFunctionsRequestOptions(personaId, token);
    const apiRequestResult = await rp(options);

    return apiRequestResult.result;
  } catch (err) {
    throw err;
  }
}

function getUserFunctionsRequestOptions(personaId, token) {
  const options = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: token,
    },
    rejectUnauthorized: false,
    uri: `http://${process.env.OAUTH_API_SVC_SERVICE_HOST}/v1/user/functions`,
    json: true,
    method: 'GET',
  };

  return options;
}

function checkIfUserHassPermissions(userFunctions, urlRequest, requestMethod) {
  let hasPermissions = false;
  // TODO: reemplazar version api con regex
  const urlRequestWithoutVersion = urlRequest.toString().replace('/v1', '');
  // TODO: analizar si se otorgan permisos por api version

  for (let index = 0; index < userFunctions.length; index+=1) {
    const item = userFunctions[index].item;
    const functionLink = item.Link;

    hasPermissions = (item.App === process.env.APP_NAME)
     && (functionLink === urlRequestWithoutVersion)
     && (item.Verbo === requestMethod);

    if (hasPermissions)
      break;
  }

  return hasPermissions;
}

exports.checkUserPermissions = checkUserPermissions;
