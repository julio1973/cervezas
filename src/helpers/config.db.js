const sql = require("mssql")
// const tracer = require("../common/tracer")({});
var Sequelize = require("sequelize")
require("dotenv").config()

const config = {
  user: process.env.DB_USER_NAME,
  password: process.env.DB_USER_PASS,
  server: process.env.DB_SERVER,
  database: process.env.DB_NAME,
  options: {
    encrypt: true
  },
  port: process.env.DB_PORT,
  dialect: "mssql",
  connectionTimeout: 60000,
  dialectOptions: {
    database: process.env.DB_NAME,
    instanceName: process.env.DB_INSTANCE_NAME
  }
};

// exports.connect = cb => {
//   sql.connect(
//     config,
//     err => {
//       if (err) {
//         tracer.trackEvent("sql.connect");
//         tracer.trackException(err);
//         throw err;
//       }

//       tracer.trackTrace("sql.connect:connected");
//       tracer.trackEvent("sql.connect:connected");

//       if (cb) {
//         cb();
//       }
//     }
//   );
// };

exports.sequelize = function() {
  return  new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER_NAME,
    process.env.DB_USER_PASS,
    {
      host: process.env.DB_SERVER,
      dialect: "mssql",
      define: {
        schema: process.env.Db_SCHEMA
      },
      define:{
        timestamps: false
      },      
      dialectOptions: {
        encrypt: true
      }
    }
  );
};
