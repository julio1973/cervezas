const restifyRouter = require('restify-router').Router

const router = new restifyRouter()


// router.add('/swagger', require('./swagger'))
// router.add('/user', require('./user'))
// router.add('/auth', require('./auth'))

router.add('/beers', require('./beers'))

module.exports = router
