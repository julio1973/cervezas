const restifyRouter = require("restify-router").Router;
const fs = require("fs");

const router = new restifyRouter();

router.get("", (req, res, next) => {
    const docText = fs.readFileSync(`${process.cwd()}/routes/v1/swagger.json`, 'utf8');
  //   res.send(200, JSON.parse(docText));
  res.send("OK")
});

module.exports = router;
