const restifyRouter = require('restify-router').Router
const router = new restifyRouter()
const userService = require('../../domain/services/userService')

router.get("", (req, res, next) => {
    userService
    .getAll()
    .then(data=> {
       res.send(200, JSON.parse(JSON.stringify(data)))
    })
    .catch(err => {
      res.send(500, err)
    })
    
});

module.exports = router