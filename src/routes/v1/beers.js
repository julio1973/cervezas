const restifyRouter = require("restify-router").Router;
const router = new restifyRouter();
const beerService = require("../../domain/services/beerService");



// Obtiene todas las cervezas
router.get("", (req, res, next) => {
  const beers = beerService.getAll();
  if (beers) {
    res.send(200, JSON.parse(JSON.stringify(beers)));
  } else {
    res.send(500, "internal error");
  }
});

// Obtiene una cerveza por id
router.get("/:id", (req, res, next) => {
  const id = req.params.id;
  const beer = beerService.getById(id);

  if (beer){
    res.send(200, JSON.parse(JSON.stringify(beer)));
  }else {
    res.send(404, 'Cerveza no existe');
  }

});

// Obtiene una cerveza por id
router.get("/:id/boxprice", (req, res, next) => {
  const id = req.params.id;
  const quantity = req.query.quantity;
  const currency = req.query.currency;
  const beer = beerService.getBoxPrice(id,quantity);

  if (beer){
    res.send(200, JSON.parse(JSON.stringify(beer)));
  }else {
    res.send(404, 'Cerveza no existe');
  }

});

// Inserta un tipo cerveza
router.post("", (req, res, next) => {
  const cerveza = req.body;
  beerService.insert(cerveza)
  res.send(200, JSON.parse(JSON.stringify(data)));

});


// Inserta un tipo cerveza
router.del("/:id", (req, res, next) => {
  const cerveza = req.params.id;
  beerService.remove(cerveza)
  res.send(200, JSON.parse(JSON.stringify(data)));

});



module.exports = router;
