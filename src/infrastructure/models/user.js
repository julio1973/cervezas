const dbConfig = require('../../helpers/config.db')
const sequelize = dbConfig.sequelize()
var Sequelize = require("sequelize")

exports.User = function(){
    return sequelize.define('Users',{
        id :  { type: Sequelize.INTEGER, primaryKey: true},
        name: { type: Sequelize.STRING, allowNull: true}                     
    }) 
}
