const _ = require("lodash");

global.cervezas = [
  {
    id: 1,
    nombre: "kustman",
    precio: 3000,
    volumen: 330,
    alchol: 3,
    caja: 18
  },
  {
    id: 2,
    nombre: "kustman torabayo",
    precio: 2800,
    volumen: 330,
    alchol: 3,
    caja: 24
  },
  {
    id: 3,
    nombre: "cristal",
    precio: 1030,
    volumen: 330,
    alchol: 3,
    caja: 12
  }
];

module.exports = (function() {
  function getAll() {
    return cervezas;
  }

  function getById(id) {
    return _.find(cervezas, cerveza => {
      return cerveza.id == id;
    });
  }

  function getBoxPrice(id, quantity) {
    const cerveza = _.find(cervezas, cerveza => {
      return cerveza.id == id;
    });

    if (cerveza) {
      const cantidad = quantity || 6;
      cerveza.precio = cantidad * cerveza.precio;
      return cerveza;
    } else {
      return null;
    }
  }

  function insert(cerveza) {
    cervezas.push(cerveza);
  }

  function remove(cerveza) {
     _.remove(cervezas, cerveza => {
      return cerveza.id == id;
    });


  }

  return {
    getAll: getAll,
    getBoxPrice: getBoxPrice,
    getById: getById,
    insert: insert,
    remove: remove
  };
})();
