var userModel = require('../models/user')


module.exports = function(){

     function getAll(){

        return  new Promise(
            (resolve, reject) => {
                userModel.User()
                .findAll()
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err);
                })
            } )  
    }

    return {
        getAll : getAll
    }

}()