
var Router = require('restify-router').Router
var restify = require('restify')
const routes = require('./src/routes/routes')
const serverUse = require('./server.use')
require('dotenv').config()


const server = restify.createServer({
    handleUncaughtExceptions: true,
  });

  serverUse.set(server, restify);
  routes.applyRoutes(server);

  server.pre((req, res, next) => {
    req.headers.accept = 'application/json';
    return next();
  });

  server.get('/api/status', (req, res) => {
    res.send('El servicio está activo.');
  });

  const fs = require('fs');
  // const secrets = require('docker-secrets-nodejs')
  server.listen(process.env.PORT, () => {
    // tracer.trackTrace(`El servidor está online ${server.name} ${server.url}`);
    // console.log(`SECRET ${secrets.get("secret_data") }`)
    // console.log(fs.readFileSync('/run/secrets/secret_data').toString())
    // console.log('env1 ' ,process.env.PFG_DB_USER_PASS)
  });
