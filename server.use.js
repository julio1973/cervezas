const restifyError = require("restify-errors");
const corsMiddleware = require("restify-cors-middleware");
// const tracer = require('./common/tracer')({});
// const authorization = require('./src/helpers/authorization')
require("dotenv").config();

const maxBodyAndFieldsSize =
  1024 * 1024 * parseInt(process.env.MAX_BODY_FIELD_SIZE, 10);

const cors = corsMiddleware({
  preflightMaxAge: 5,
  origins: ["*"],
  allowHeaders: ["Authorization", "Content-Type"],
  exposeHeaders: ["API-Token-Expiry"]
});

const init = (server, restify) => {
  server.pre(cors.preflight);
  server.use(cors.actual);
  server.use(
    restify.plugins.bodyParser({
      maxBodySize: maxBodyAndFieldsSize,
      maxFieldsSize: maxBodyAndFieldsSize
    })
  );
  server.use((req, res, next) => {
    if (
      req.url === "/v1/auth/authenticate" ||
      req.url === "/api/status" ||
      req.url === "/v1/swagger"
    ) {
      next();
      return;
    } else {
      next();
      return
    }
  });

  // server.use((req, res, next) => {
  //   if (
  //     req.url === '/v1/auth/authenticate' ||
  //     req.url === '/api/status' ||
  //     req.url === '/v1/swagger'
  //   ) {
  //     next();
  //     return;
  //   }else{
  //     next()
  //   }

  //   // let token = req.header('Authorization')
  //   // if (!token && req.query('token'))
  //   //   token = req.query('token').replace('token=', '')
  //   //    next()
  //   // // if (token) {
  //   // //   oauth
  //   // //     .verify(token)
  //   // //     .then((data) => data)
  //   // //     .then((user) => {
  //   // //       req.user = user;
  //   // //       req.token = token;

  //   // //       return authorization.checkUserPermissions(req, user);
  //   // //     })
  //   // //     .then((hasPermissions) => {
  //   // //       if (!hasPermissions) {
  //   // //         res.send(403, { error: "Insuficientes permisos para ejecutar la operación" });
  //   // //         return;
  //   // //       }

  //   // //       next();
  //   // //     })
  //   // //     .catch((exc) => {
  //   // //       console.log(exc);
  //   // //       res.send(exc);
  //   // //     });
  //   // // } else {
  //   // //   res.send(new restifyError.UnauthorizedError());
  //   // // }
  // });

  server.defaultResponseHeaders = data => {
    this.header("Content-Type", "application/json");
  };

  // server.use(
  //   restify.plugins.throttle({
  //     burst: 100,
  //     rate: 50,
  //     ip: true,
  //     overrides: {
  //       '192.168.1.1': {
  //         rate: 0,
  //         burst: 0,
  //       },
  //     },
  //   })
  // );

  server.use(restify.plugins.conditionalRequest());

  server.use(restify.plugins.queryParser());

  server.on("restifyError", (req, res, err, callback) => {
    // tracer.trackException(err);
    console.log(err);
    return callback();
  });

  process.on("unhandledRejection", (reason, promise) => {
    // tracer.trackException(new Error(reason.stack));
    console.log(err);
  });

  process.on("uncaughtException", err => {
    // tracer.trackException(err);
    console.log(err);
    process.exit(1);
  });
};

exports.set = init;
